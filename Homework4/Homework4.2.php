//while, do while, for, foreach.
<h3>Zadanie 1</h3>
<?php

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$count = 0;
foreach ($arr as $value) {
    $count++;
}

$arrChange1 = [];
$arrChange2 = [];
$arrChange3 = [];
$arrChange4 = [];

//for
for ($i = 1; $i <= $count; $i++) {
    $arrChange1[$i] = $arr[$count - $i];
}
print_r($arrChange1);
echo "</br>";

//While
$i = 0;
while ($i < $count) {
    $i++;
    $arrChange2[$i] = $arr[$count - $i];
}
print_r($arrChange2);
echo "</br>";

//do while
$i = 1;
do {
    $arrChange3[$i] = $arr[$count - $i];
    $i++;
} while ($i <= $count);
print_r($arrChange3);
echo "</br>";

//foreach
$i = 0;
foreach ($arr as $key => $value) {
    $arrChange1[$i] = $arr[$count - $i];
    $i++;
}
print_r($arrChange3);

echo "<hr>";


?>
<h3>Zadanie 2</h3>
<?php

$numbers = [44, 12, 11, 7, 1, 99, 43, 5, 69];

// While
$arr_count = 0;
while ($arr_count >= 0) {
    if (isset($numbers[$arr_count])) {
        $arr_count++;
    } else {
        break;
    }
}
// new array
while ($arr_count-- != 0) {
    $numbers_reverse[] = $numbers[$arr_count];
}
echo '<pre>';
print_r($numbers_reverse);
echo '</pre>';

// do while
$arr_count = 0;
do {
    if (isset($numbers[$arr_count])) {
        $arr_count++;
    } else {
        break;
    }
} while ($arr_count >= 0);
// new array
do {
    $numbers_reverse_1[] = $numbers[--$arr_count];
} while ($arr_count != 0);
echo '<pre>';
print_r($numbers_reverse_1);
echo '</pre>';

// For
for ($i = 0; $i >= 0; ++$i) {
    if (isset($numbers[$i])) {
        $arr_count = $i;
    } else {
        break;
    }
}
// new array
for ($i = $arr_count; $i != -1; --$i) {
    $numbers_reverse_2[] = $numbers[$i];
}
echo '<pre>';
print_r($numbers_reverse_2);
echo '</pre>';

// foreach
$count = -1;
foreach ($numbers as $number) {
    $count++;
}
// new array
foreach ($numbers as $number) {
    $numbers_reverse_3[] = $numbers[$count];
    $count--;
}
echo '<pre>';
print_r($numbers_reverse_3);
echo '</pre>'


?>
<h3>Zadanie 3</h3>
<?php

$str = 'Hi I am Alex';
$len = 0;
$i = 0;
while (isset($str[$i])) {
    $len++;
    $i++;
}
echo $len . "</br>";
$str1 = '';
$str2 = '';
$str3 = '';
$str4 = '';

for ($i = 0; $i <= $len; $i++) {
    $str1[$i] = $str[$len - $i];
}
echo $str1 . "</br>";

$i = 0;
while ($i < $len) {
    $i++;
    $str2[$i] = $str[$len - $i];
}
echo $str2 . "</br>";

$i = 1;
do {
    $str3[$i] = $str[$len - $i];
    $i++;
} while ($i <= $len);
echo $str3;
echo "<hr>";


?>
<h3>Zadanie 4</h3>
<?php

$str = 'Hi I am ALex';
$str1 = '';
$str2 = '';
$str3 = '';
$str4 = '';

//for
for ($i = 0; $i < $len; $i++) {
    $str1[$i] = strtolower($str[$i]);
}
echo $str1 . "</br>";

//while
$i = 0;
while ($i < $len + 1) {
    $str2[$i] = strtolower($str[$i]);
    $i++;
}
echo $str2 . "</br>";

//do while
$i = 0;
do {
    $str3[$i] = strtolower($str[$i]);
    $i++;
} while ($i <= $len);
echo $str3;
echo "<hr>";


?>
<h3>Zadanie 5</h3>
<?php

$str = 'Hi I am ALex';
$str1 = '';
$str2 = '';
$str3 = '';
$str4 = '';

//for
for ($i = 0; $i < $len; $i++) {
    $str1[$i] = strtoupper($str[$i]);
}
echo $str1 . "</br>";

//while
$i = 0;
while ($i < $len + 1) {
    $str2[$i] = strtoupper($str[$i]);
    $i++;
}
echo $str2 . "</br>";

//dp while
$i = 0;
do {
    $str3[$i] = strtoupper($str[$i]);
    $i++;
} while ($i <= $len);
echo $str3;
echo "<hr>";


?>

<h3>Zadanie 7</h3>
<?php

$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];

// while
$count = 0;
while ($count >= 0) {
    if (isset($names[$count])) {
        $count_letters = 0;
        while ($count_letters >= 0) {
            if (isset($names[$count][$count_letters])) {
                if (isset($ABCabc[$names[$count][$count_letters]])) {
                    $names[$count][$count_letters] = $ABCabc[$names[$count][$count_letters]];
                }
            } else {
                break;
            }
            $count_letters++;
        }
    } else {
        break;
    }
    $count++;
}
echo '<pre>';
print_r($names);
echo '</pre>';

// do while
$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$count = 0;
do {
    if (isset($names[$count])) {
        $count_letters = 0;
        do {
            if (isset($names[$count][$count_letters])) {
                if (isset($ABCabc[$names[$count][$count_letters]])) {
                    $names[$count][$count_letters] = $ABCabc[$names[$count][$count_letters]];
                }
            } else {
                break;
            }
            $count_letters++;
        } while ($count_letters >= 0);
    } else {
        break;
    }
    $count++;
} while ($count >= 0);
echo '<pre>';
print_r($names);
echo '</pre>';

// for
$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
for ($i = 0; $i >= 0; $i++) {
    if (isset($names[$i])) {
        for ($x = 0; $x >= 0; $x++) {
            if (isset($names[$i][$x])) {
                if (isset($ABCabc[$names[$i][$x]])) {
                    $names[$i][$x] = $ABCabc[$names[$i][$x]];
                }
            } else {
                break;
            }
        }
    } else {
        break;
    }
}
echo '<pre>';
print_r($names);
echo '</pre>';

// foreach
$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
foreach ($names as $key => $name) {
    $name_new = null;
    $arr_name = null;
    $arr_count = 0;
    while ($arr_count >= 0) {
        if (isset($name[$arr_count])) {
            $arr_name[] = $name[$arr_count];
            $arr_count++;
        } else {
            break;
        }
    }
    foreach ($arr_name as $value) {
        if (isset($ABCabc[$value])) {
            $name_new .= $ABCabc[$value];
        } else {
            $name_new .= $value;
        }
    }
    $names_new[$key] = $name_new;
}
echo '<pre>';
print_r($names_new);
echo '</pre>';


?>
<h3>Zadanie 8</h3>
<?php

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$count = 0;

foreach ($arr as $value) {
    $count++;
}

$arrChange1 = [];
$arrChange2 = [];
$arrChange3 = [];
$arrChange4 = [];

//for
for ($i = 0; $i < $count; $i++) {
    $arrChange1[$i] = strtoupper($arr[$i]);
}
print_r($arrChange1);
echo "</br>";

//while
$i = 0;
while ($i < $count) {
    $arrChange2[$i] = strtoupper($arr[$i]);
    $i++;
}
print_r($arrChange2);
echo "</br>";

// do while
$i = 0;
do {
    $arrChange3[$i] = strtoupper($arr[$i]);
    $i++;
} while ($i < $count);
print_r($arrChange3);
echo "</br>";

$i = 0;

//foreach
foreach ($arr as $key => $value) {
    $arrChange4[$key] = strtoupper($value);
}
print_r($arrChange4);
echo "<hr>";


?>
<h3>Zadanie 9</h3>
<?php

$num = 1234678;
$str_num = (string)$num;

// while
$arr_count = 0;
$num_new = null;
while ($arr_count >= 0) {
    if (isset($str_num[$arr_count])) {
        $arr_count++;
    } else {
        break;
    }
}

while ($arr_count-- != 0) {
    $num_new .= $str_num[$arr_count];
}
echo  (integer)$num_new . '<br>';

// do while

$arr_count = 0;
$num_new = null;
do {
    if (isset($str_num[$arr_count])) {
        $arr_count++;
    } else {
        break;
    }
} while ($arr_count >= 0);

do {
    $num_new .= $str_num[--$arr_count];
} while ($arr_count != 0);
echo (integer)$num_new . "<br>";

// for

$num_new = null;
for ($i = 0; $i >= 0; ++$i) {
    if (isset($str_num[$i])) {
        $arr_count = $i;
    } else {
        break;
    }
}

$str_new = null;
for ($i = $arr_count; $i != -1; --$i) {
    $num_new .= $str_num[$i];
}
echo (integer)$num_new . "<br>";

// foreach
$arr_count = 0;
$num_new = null;
while ($arr_count >= 0) {
    if (isset($str_num[$arr_count])) {
        $arr_str[] = $str_num[$arr_count];
        $arr_count++;
    } else {
        break;
    }
}

$count = -1;
foreach ($arr_str as $value) {
    $count++;
}

$str_new = null;
foreach ($arr_str as $value) {
    $num_new .= $arr_str[$count];
    $count--;
}
echo (integer)$num_new . "<br>";


?>
<h3>Zadanie 10</h3>
<?php

$num_arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];

// while
$count_arr_elements = 0;
$count = 0;
$count_all_num = 0;
$num_arr_new = null;

while ($count_arr_elements >= 0) {
    if (isset($num_arr[$count_arr_elements])) {
        $count_arr_elements++;
    } else {
        break;
    }
}

while ($count < $count_arr_elements) {
    $count_arr = 0;
    while ($count_arr < $count_arr_elements) {
        if ($num_arr[$count_arr] == $count_all_num) {
            $num_arr_new[] = $num_arr[$count_arr];
            $count++;
        }
        $count_arr++;
    }
    $count_all_num++;
}

while ($count-- != 0) {
    $num_arr_reverse[] = $num_arr_new[$count];
}
echo '<pre>';
print_r($num_arr_reverse);
echo '</pre>';

// do while
$count_arr_elements = 0;
$count = 0;
$count_all_num = 0;
$num_arr_new = null;
$num_arr_reverse = null;
do {
    if (isset($num_arr[$count_arr_elements])) {
        $count_arr_elements++;
    } else {
        break;
    }
} while ($count_arr_elements >= 0);

do {
    $count_arr = 0;
    do {
        if ($num_arr[$count_arr] == $count_all_num) {
            $num_arr_new[] = $num_arr[$count_arr];
            $count++;
        }
        $count_arr++;
    } while ($count_arr < $count_arr_elements);
    $count_all_num++;
} while ($count < $count_arr_elements);

do {
    $num_arr_reverse[] = $num_arr_new[--$count];
} while ($count != 0);
echo '<pre>';
print_r($num_arr_reverse);
echo '</pre>';

// for

$count = 0;
$arr_count = 0;
$num_arr_new = null;
$num_arr_reverse = null;
for ($i = 0; $i >= 0; ++$i) {
    if (isset($num_arr[$i])) {
        $arr_count++;
    } else {
        break;
    }
}

for ($i = 0; $i >= 0; ++$i) {
    if ($count < $arr_count) {
        for ($x = 0; $x < $arr_count; ++$x) {
            if ($num_arr[$x] == $i) {
                $num_arr_new[] = $num_arr[$x];
                $count++;
            }
        }
    } else {
        break;
    }
}
for ($i = --$arr_count; $i >= 0; --$i) {
    $num_arr_reverse[] = $num_arr_new[$i];
}
echo '<pre>';
print_r($num_arr_reverse);
echo '</pre>';

// foreach
$count = 0;
$count_arr = 0;
$count_all = 0;
$num_arr_new = null;
$num_arr_reverse = null;
foreach ($num_arr as $value) {
    $count_arr++;
}
while ($count < $count_arr) {
    foreach ($num_arr as $value) {
        if ($value == $count_all) {
            $num_arr_new[] = $value;
            $count++;
        }
    }
    $count_all++;
}

foreach ($num_arr as $value) {
    $num_arr_reverse[] = $num_arr_new[--$count_arr];
}
echo '<pre>';
print_r($num_arr_reverse);
echo '</pre>';


?>

