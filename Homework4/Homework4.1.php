<?php
// Задание простых чисел.
for ($i = 1; $i < 100; $i++) {
    for ($j = 2; $j < $i; $j++) {
        if ($i % $j == 0) {
            break;
        } elseif ($i == $j + 1) {
            echo $i . "<br>";
        }
    }
}
?>
<h3>Zadanie 2</h3>

<?php
$k = 0;
$a = 1;
while ($a <= 100) {
    if ($a % 2 == 0) {
        $k++;
    }
    $a++;
}
echo $k . "<br>";

?>

<h3>Zadanie 3</h3>

<?php

$a = 0;
$i = 1;
while ($i <= 100) {
    $k = 1;
    while ($k <= 5) {
        $k++;
        $a++;
    }
    $i++;
}
echo $a;

?>

<h3>Zadanie 4</h3>

<?php

$a = 0;
echo '<br><hr><table border="1">';
while ($a < 3) {
    $c = 0;
    echo $a ? '<tr style="background-color: white;">' : '<tr>';
    while ($c < 5) {
        if ($c % 4 == 0) {
            $r = rand(0, 255);
            $g = rand(0, 255);
            $b = rand(0, 255);
            echo "<td style='background-color: rgb($r,$g,$b)'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
        } else {
            echo "<td>&nbsp;&nbsp;&nbsp;</td>";
        }
        $c++;
    }
    echo "</tr>";
    $a++;
}
echo '</table><br><hr>';

?>

    <h3>Zadanie 5</h3>
<?php
$month = rand(1, 12);
if ($month >= 3 && $month <= 5) {
    echo 'Сейчас Весна!!! (' . $month . ')';
} else if ($month >= 6 && $month <= 8) {
    echo 'Сейчас Лето!!! (' . $month . ')';
} else if ($month >= 9 && $month <= 11) {
    echo 'Сейчас Осень!!! (' . $month . ')';
}
else if ($month >= 2 && $month <= 12) {
    echo 'Сейчас Зима!!! (' . $month . ')';
}
?>
<h3>Zadanie 6</h3>
<?php
$liters = 'abcde';
echo $liters[0] == 'a' ? "Yes" : 'No' . "<br>";
?>
<h3>Zadanie 7</h3>
<?php
$numbers = '42135';
if ($numbers[0] == '1' || $numbers[0] == '2'|| $numbers[0] == '3'){
    echo "Yes";
}
else{
    echo "No";
}
?>
<h3>Zadanie 8</h3>
<?php
$test = true;
if ($test){
    echo "Верно";
}
else{
    echo "Не верно";
}
echo "<pre>";
if($test){
    echo "Verno";
}
else if($test == false){
    echo "Ne verno";
}
?>
<h3>Zadanie 9</h3>
<?php

$language = 'eng';
$week_ru = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
$week_eng = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
if ($language == 'ru') {
    echo '<pre>';
    print_r($week_ru);
    echo '</pre>';
} else {
    echo '<pre>';
    print_r($week_eng);
    echo '</pre>';
}
echo $language == 'ru' ? print_r($week_ru) : print_r($week_eng);
echo '<br><hr>';
?>
<h3>Zadanie 10</h3>

<?php

$cloсk = rand(0, 59);
echo 'Сейчас: '  . $cloсk . ' минут<br>';
echo $cloсk <= 25 ? 'Первая четверть часа' :
    ($cloсk > 25 && $cloсk <= 50 ? 'Вторая четверть часа' :
        ($cloсk > 50 && $cloсk <= 75 ? 'Третья четверть часа' :
            ($cloсk > 75 ? 'Четвертая четверть часа' : '')));

echo '<br>';

if ($cloсk <= 25) {
    echo 'Первая четверть часа';
} elseif ($cloсk > 25 && $cloсk <= 50) {
    echo 'Вторая четверть часа';
} elseif ($cloсk > 50 && $cloсk <= 75) {
    echo 'Третья четверть часа';
} else {
    echo 'Четвертая четверть часа';
}
?>




