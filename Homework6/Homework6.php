<?php

function dd($arr) {
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
}
echo "<hr>";

//Zadanie 1
// 1 Найти минимальное и максимальное среди 3 чисел

$getMin = function ($num1, $num2, $num3) {
    $arr = [$num1, $num2, $num3];
    $min = $arr[0];
    for ($i = 0; $i <= 2; $i++) {
        if ($arr[$i] <= $min) {
            $min = $arr[$i];
        }
    }
    return $min;
};
$getMin2 = fn($num1, $num2, $num3) => $getMin($num1, $num2, $num3);

$getMax = function ($num1, $num2, $num3) {
    $arr = [$num1, $num2, $num3];
    $max = $arr[0];
    for ($i = 0; $i <= 2; $i++) {
        if ($arr[$i] >= $max) {
            $max = $arr[$i];
        }
    }
    return $max;
};

$getMax2 = fn($num1, $num2, $num3) => $getMax($num1, $num2, $num3);

// Zadanie 2
$calcSquare1 = function ($len, $wid) {
    return $len * $wid;
};
echo $calcSquare1(5, 5) . "</br>";

//strel

$calcSquare1 = fn($len, $wid) => $len * $wid;
echo $calcSquare1(7, 7) . "</br>";

//zadanie 3

$calcPifagorNum = function ($a, $b) {
    return sqrt($a ** 2 + $b ** 2);

};
echo $calcPifagorNum(7, 7) . "</br>";

//strel
$calcPifagorNum2 = fn($a, $b) => sqrt($a ** 2 + $b ** 2);
echo $calcPifagorNum2(7, 7) . "</br>";


//zadanie 4

$perimetr1 = function ($len1, $Len2, $len3) {
    return $len1 + $Len2 + $len3;
};
echo $perimetr1(12, 5, 17) . "</br>";


//strel
$perimetr2 = fn($len1, $Len2, $len3) => $len1 + $Len2 + $len3;
echo $perimetr2(5, 12, 10) . "</br>";

//zadanie 5

$discriminant1 = function ($a, $b, $c) {
    return $b ** 2 - 4 * $a * $c;
};
echo $discriminant1(1, 5, 2) . "</br>";


//strel
$discriminant2 = fn($a, $b, $c) => $b ** 2 - 4 * $a * $c;
echo $discriminant2(1, 5, 2) . "</br>" . "</br>";

//zadanie 6

$even1 = function () {
    for ($i = 0; $i < 100; $i++) {
        if ($i % 2 == 0) {
            echo $i . ' ';
        }
    }
};
$even1();
echo "</br>" . "</br>";


//strel
function calcEven()
{
    for ($i = 0; $i < 100; $i++) {
        if ($i % 2 == 0) {
            echo $i . ' ';
        }
    }
}
$even2 = fn() => calcEven();
$even2();
echo "</br>" . "</br>";

//zadanie 7

$odd1 = function () {
    for ($i = 0; $i < 100; $i++) {
        if ($i % 2 == 1) {
            echo $i . ' ';
        }
    }
};
$odd1();
echo "</br>" . "</br>";


//strel
function calcOdd()
{
    for ($i = 0; $i < 100; $i++) {
        if ($i % 2 == 1) {
            echo $i . ' ';
        }
    }
}
$odd2 = fn() => calcOdd();
$odd2();
echo "</br>" . "</br>";

//zadanie 8

$isRepeat1 = function ($arr) {
    $is = 0;
    $count = count($arr);
    for ($i = 0; $i < $count; $i++){
        for ($j = 0; $j < $count; $j++){
            if ($arr[$i] == $arr[$j] && $i != $j){
                $is = 1;
                break;
            } else {
                $is = 0;
            }
        }
    }
    return $is;
};
$array1 = [1, 2, 4, 6, 23, 56, 1, 8, 23];
echo $isRepeat1($array1);
echo "</br>" . "</br>";

//strel
function isRepeate($arr) {
    $is = 0;
    $count = count($arr);
    for ($i = 0; $i < $count; $i++){
        for ($j = 0; $j < $count; $j++){
            if ($arr[$i] == $arr[$j] && $i != $j){
                $is = 1;
                break;
            } else {
                $is = 0;
            }
        }
    }
    return $is;
}
$isRepeat2 = fn($array = [1, 2, 4, 6, 23, 56, 1, 8, 23]) => isRepeate($array);
echo $isRepeat2();

//zadanie 9
$sortArr = function ($arr, $sortType = 'asc') {
    $len = count($arr);
    if ($sortType !== 'asc' || $sortType !== 'desc') {
        $sortType = 'asc';
    }
    for ($i = 0; $i < $len; $i++) {
        for ($j = 0; $j < $len - 1; $j++) {
            if (($arr[$j] >= $arr[$j + 1] && $sortType == 'asc') ||
                ($arr[$j] <= $arr[$j + 1] && $sortType == 'desc')) {
                $temp = $arr[$j];
                $arr[$j] = $arr[$j + 1];
                $arr[$j + 1] = $temp;
            }
        }
    }
    return $arr;
};

$array = [2, 4, 12, 7, 45, 100, 34, 1];
print_r($sortArr($array));

//strel
function sortArray($arr, $sortType = 'asc') {
    $len = count($arr);
    if ($sortType !== 'asc' || $sortType !== 'desc') {
        $sortType = 'asc';
    }
    for ($i = 0; $i < $len; $i++) {
        for ($j = 0; $j < $len - 1; $j++) {
            if (($arr[$j] >= $arr[$j + 1] && $sortType == 'asc') ||
                ($arr[$j] <= $arr[$j + 1] && $sortType == 'desc')) {
                $temp = $arr[$j];
                $arr[$j] = $arr[$j + 1];
                $arr[$j + 1] = $temp;
            }
        }
    }
    return $arr;
}

$sortArr2 = fn($arr = [2, 4, 12, 7, 45, 100, 34, 1]) => sortArray($arr);
print_r($sortArr2());

echo "<hr>";
